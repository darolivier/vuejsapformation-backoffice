import Vue from 'vue'
import VueRouter from 'vue-router'
import Homepage from '../components/Homepage.vue'
import Products from '../components/ProductsList.vue'
import ProductSheet from '../components/ProductSheet.vue'
import AllCommands from '../components/AllCommands.vue'
import CreateProduct from '../components/ProductCreation.vue'
import ListUsers from '../components/UsersList.vue'
import UserSheet from '../components/UserSheet.vue'
import CreateUser from '../components/UserCreation.vue'
import UserEdit from '../components/UserEdit.vue'
import ProductModify from '../components/ProductEdit.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/accueil',
    name: 'homepage',
    component: Homepage
  },
  {
    path: '/products',
    name: 'products',
    component: Products,
    
  },
  {
    path: '/productSheet/:productId',
    name: 'productSheet',
    component: ProductSheet,
  },
  {
    path: '/allCommands',
    name: 'allCommands',
    component: AllCommands,
  },
  {
    path: '/createProduct',
    name: 'createProduct',
    component: CreateProduct,
  },
  {
    path: '/listUsers',
    name: 'listUsers',
    component: ListUsers,
  },
  {
    path: '/user-sheet/:userId',
    name: 'userSheet',
    component: UserSheet,
  },
  {
    path: '/createUser',
    name: 'createUser',
    component: CreateUser,
  },
  {
    path: '/user-edit/:userId',
    name: 'userEdit',
    component: UserEdit,
  },
  {
    path: '/product-modify/:productId',
    name: 'productModify',
    component: ProductModify,
  },
 
]

const router = new VueRouter({
  routes
})

export default router
