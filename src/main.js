import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  methods: {
    getImage(id) {
      var images = require.context('./assets/computers', false, /\.jpg$/)
      return images('./images' +" ("+ id + ")"  + ".jpg")
    }
  },
  beforeCreate: function () {
    this.$store.dispatch('setProductsToStore')
  }
}).$mount('#app')
