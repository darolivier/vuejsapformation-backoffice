import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    users: [],
    products: [],
    
  },
  mutations: {
    SET_PRODUCTS(state, products) { 
      state.products = products;
    },
    SET_USERS(state, users) { 
      state.users = users;
    },
    
  },
  getters: {
    // findArticle: (state) => (id) => {
    //   return state.cartProducts.filter(product => product.id == id)
    // },
  },
  actions: {
    setProductsToStore() {
      fetch('../../data/db_products.json') 
      .then(function(response) {
        return response.json();
      })
      .then(response => {
         this.commit('SET_PRODUCTS', response.products);
      })   
    },
    setUsersToStore() {
      fetch('../../data/db_users.json') 
      .then(function(response) {
        return response.json();
      })
      .then(response => {
         this.commit('SET_USERS', response.users);
      })   
    },
    DELETE_ARTICLE({state},index) {
      console.log(state, this)
        state.cartProducts.splice(index, 1);
    },
}
})